-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2019 at 11:35 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `key_v` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `ttl` int(2) NOT NULL DEFAULT 5,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `key_v`, `value`, `ttl`, `created_at`, `updated_at`) VALUES
(1, 'k_1', '1', 5, '2019-12-30 07:37:31', '2019-12-30 04:16:22'),
(2, 'k_2', '2', 5, '2019-12-30 07:37:31', '2019-12-30 04:16:22'),
(3, 'k_3', '3', 5, '2019-12-30 07:37:31', '2019-12-30 04:16:22'),
(4, 'k_4', '4', 5, '2019-12-30 07:37:31', '2019-12-30 04:16:22'),
(5, 'k_5', '5', 5, '2019-12-30 07:37:31', '2019-12-30 04:16:22'),
(22, 'k_6', '19', 5, '2019-12-30 07:40:18', '2019-12-30 04:16:22'),
(23, 'k_7', '10', 5, '2019-12-30 07:40:18', '2019-12-30 04:16:22'),
(24, 'k_8', '8', 5, '2019-12-30 07:40:19', '2019-12-30 04:16:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
