<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;

class TestAPI extends Controller
{
    public $successStatus = 200;

    public function getAllValues()
    {
        if (isset($_GET['keys'])) {
            $keys = explode(",", $_GET['keys']);
            $data = Store::select('key_v', 'value')->whereIn('key_v', $keys)->get();
            
            for ($i = 0; $i < sizeof($keys); $i++) {
                Store::where('key_v', $keys[$i])->update(['ttl' => 5]);
            }
        } else {
            $data = Store::all('key_v', 'value');
            Store::where('ttl', '<=', 5)->update(['ttl' => 5]);
        }
        return response()->json($data, $this->successStatus);
    }

    public function storeValues(Request $request)
    {
        $data = $request->all();
        foreach ($data as $key => $d) {
            $value = array(
                array('key_v' => $key, 'value' => $d)
            );
            Store::insert($value);
        }
        return response()->json("Successfully Added Values", $this->successStatus);
    }

    public function updateValues(Request $request)
    {
        $data = $request->all();
        foreach ($data as $key => $d) {
            Store::where('key_v', $key)->update(['value' => $d, 'ttl' => 5]);
        }
        return response()->json("Successfully Updated Values", $this->successStatus);
    }
}
